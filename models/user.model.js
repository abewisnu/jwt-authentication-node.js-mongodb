
//user.model.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  name: {
    type: String,
  },
  email: {
    type: String
  },
  password: {
    type: String
  }
}, {
  createdAt: 'created_at',
  updatedAt: 'updated_at',
});

module.exports = mongoose.model('User', userSchema);
