// routes/user.js

const express = require('express');
const router = express.Router();
const jwtMiddleware = require('../middleware/jwt'); // Pastikan path sesuai dengan struktur proyek Anda
const User = require('../models/User'); // Gantilah path dengan lokasi model User Anda

// Rute untuk mendapatkan informasi pengguna saat ini
router.get('/user', jwtMiddleware, async (req, res) => {
  try {
    // Dapatkan informasi pengguna berdasarkan ID pengguna yang ada dalam token JWT
    const user = await User.findById(req.user.id);

    // Jika pengguna ditemukan, kirimkan informasi pengguna dalam respons
    if (user) {
      res.json({
        id: user._id,
        name: user.name,
        //email: user.email, // Sesuaikan dengan properti pengguna Anda
        //Tambahkan informasi lain yang ingin Anda kirimkan
      });
    } else {
      // Jika pengguna tidak ditemukan (seharusnya tidak terjadi), kirimkan pesan kesalahan
      res.status(404).json({ error: 'Pengguna tidak ditemukan' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Terjadi kesalahan saat mengambil informasi pengguna' });
  }
});

module.exports = router;
