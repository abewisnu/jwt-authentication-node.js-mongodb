//auth.js
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user.model');

// Registrasi pengguna
router.post('/register', async (req, res) => {
  try {
    const { name, email, password } = req.body;
    const hashedPassword = await bcrypt.hash(password, 10);
    const user = new User({ name, email, password: hashedPassword });
    await user.save();
    res.status(201).json({ message: 'Pengguna berhasil terdaftar' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Gagal mendaftarkan pengguna' });
  }
});

// Masuk pengguna
router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({ email });
    if (!user) {
      return res.status(401).json({ error: 'Pengguna tidak ditemukan' });
    }
    const validPassword = await bcrypt.compare(password, user.password);
    if (!validPassword) {
      return res.status(401).json({ error: 'Password salah' });
    }
    const token = jwt.sign({ userId: user._id }, 'rahasia-rahasia', { expiresIn: '1h' });
    res.json({ token });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Gagal masuk' });
  }
});

module.exports = router;
